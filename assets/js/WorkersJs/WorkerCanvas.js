importScripts('../html2canvas.min.js');
onmessage = function (oEvent) {

    html2canvas(document.querySelector(oEvent.data),{
        useCORS: true,
        allowTaint: false,
        logging : true,
        ignoreElements: (node) => {
            return node.classList.contains('gl-star-rating-stars'); 
        }
    }).then( 
        canvas => postMessage(canvas.toDataURL('image/png'))
    );   
}