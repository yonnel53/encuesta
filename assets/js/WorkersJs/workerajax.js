onmessage = function (oEvent) {
    var fetch_params = JSON.parse(oEvent.data);
    
    fetch(fetch_params.url,{
        method : fetch_params.method,
        body   : fetch_params.body,
        header : fetch_params.header
    })
    .then(
        response => postMessage(true)
    )
    .catch(
        error => postMessage(false)
    )

};