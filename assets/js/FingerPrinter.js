class FingerPrinter{

    getPrinterSave(){
        var self   = this;
        var anonymous = function(canvas){
            return {
                'Browser'    : self.getBrowserClient(),
                'DimensionS' : self.getDimensionScreen(),
                'OperativeS' : self.getVersionClient(),
                'AcronS'     : self.getAcronClient(),
             //   'PrintUser'    : canvas,
                'TimeNow'      : self.getNowTime(),
                'DomainOirign' : self.getDomain(),
                'id'           : self.GenerateMyKey()
            };
        }
        return self.getCanvasViewUser(anonymous)   
    }

    /*
    GetRelativeId(){
        var navigator_info = window.navigator;
        var screen_info = window.screen;
        var uid = navigator_info.mimeTypes.length;
        uid += navigator_info.userAgent.replace(/\D+/g, '');
        uid += navigator_info.plugins.length;
        uid += screen_info.height || '';
        uid += screen_info.width || '';
        uid += screen_info.pixelDepth || '';
        uid += this.getNowTime();
        return uid;
    }*/

    getBrowserClient(){
        var N = navigator.appName, ua = navigator.userAgent, tem;
        var M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);

        if (M && (tem = ua.match(/version\/([\.\d]+)/i)) != null) 
            M[2] = tem[1];
        
        M = M ? [M[1], M[2]] : [N, navigator.appVersion, '-?'];

        var browser = ( M[0].toLowerCase() == "netscape") ?
            "IE11":
            (
                ua.toLowerCase().indexOf('edge')!=-1
            )?
            "edge": M[0].toLowerCase();

        var browserVersion = parseInt(M[1], 10);

        return  browser+'-'+browserVersion
        
    }

    getDimensionScreen(){
        return  [screen.height, screen.width, screen.colorDepth].join('x');
    }

    getVersionClient(){
        return navigator.appVersion;
    }

    getAcronClient(){
        return navigator.platform;
    }

    async getCanvasViewUser(callback){

        return html2canvas(document.querySelector('.form_save'),{
            useCORS: true,
            allowTaint: false,
            logging : true,
            ignoreElements: (node) => {
                return node.classList.contains('gl-star-rating-stars'); 
            }
        }).then( 
            canvas => callback(canvas.toDataURL('image/png'))
        );   
    }

    getDomain(){
        return location.hostname;
    }


    checksum(str) {
        var self = this;

        var hash = 5381, i = str.length;
    
        str = self.map(str,item => typeof item != 'object' || typeof item != 'string' ? item.toString() : item  );
        
        while (i--) hash = (hash * 33) ^ str[i].charCodeAt(i);
    
        return hash >>> 0;
    }


    map(arr, fn){
        var i = 0, len = arr.length, ret = [];
        while(i < len){
            ret[i] = fn(arr[i++]);
        }
        return ret;
    }

    GenerateMyKey(){
        var self = this;
        var time = new Date()

        return self.checksum([

            self.getBrowserClient(),

            self.getAcronClient(),

            self.getVersionClient(),

            self.getDimensionScreen(),

            self.getDomain(),

            time.getTimezoneOffset(),
            
            !!window.sessionStorage,

            !!window.localStorage,

            self.map(navigator.plugins, function (plugin) {
                return [
                    plugin.name,
                    plugin.description,
                    self.map(plugin, function (mime) {
                        return [mime.type, mime.suffixes].join('~');
                    }).join(',')
                ].join("::");

            }).join(';'),

        ]);
    }

    getNowTime(){
       return new Date().getTime()
    }

}