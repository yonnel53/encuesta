    
    const Axios = (function(fetch_params,success = '' , error = '',before = ''){

        var default_params = {
            method     :  "POST",
            headers:{
                'Content-Type': 'application/json'
            }
        }

        var options_fech = Object.assign({},default_params, fetch_params);
        var url_fech     = options_fech.url;

        if(!url_fech)
            throw 'Debe definir una url en las opciones de la peticion';

        
        function WorkerAjax(){
            var worker = new Worker('../../assets/js/WorkersJs/workerajax.js');

            worker.postMessage(
                JSON.stringify(options_fech)
            );

            worker.onmessage = function (oEvent) {
                if(oEvent.data)
                    return (success != '') ? success() : '';

                return (success != '') ? error() : '';
            };

        }

        return WorkerAjax();
    })