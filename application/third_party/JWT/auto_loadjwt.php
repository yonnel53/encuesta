<?php
namespace JWT;

require_once APPPATH ."/third_party/JWT/BeforeValidException.php";
require_once APPPATH ."/third_party/JWT/ExpiredException.php";
require_once APPPATH ."/third_party/JWT/SignatureInvalidException.php";
require_once APPPATH ."/third_party/JWT/JWT.php";
use \Firebase\JWT\JWT;

class PHPJWT
{
	public $key_api   = 'ATK#\6Gv9u`n2';
	public $admin_api = 'ADM#\6Gv9u`n2';

    public function encode_func($token = [],$admin = false){

    	$token = [
	    	'iat' => time(),
		    'exp' => !$admin ? time() + (30 * 24 * 60 * 60) : time() + (60 * 60 * 24 * 360),
		    'data' => $token
	    ];

		return !$admin ? 
				JWT::encode($token,base64_decode($this->key_api))  :
				JWT::encode($token,base64_decode($this->admin_api));
	} 


    public function decode_func($jwt = '',$admin = false){

		if(is_numeric($jwt) )
			return false;

		try{
			return !$admin ? 
				JWT::decode(trim($jwt),base64_decode($this->key_api),array('HS256')) : 
				JWT::decode($jwt,base64_decode($this->admin_api),array('HS256'));
		}catch(\Exception $e){
			return false;
		}

	} 
	
	/*admin token*/
	/*
	public function encode_admin($token = []){

    	$token = [
	    	'iat' => time(),
		    'exp' => time() + (60 * 60 * 24 * 360),
		    'data' => $token
	    ];

    	return JWT::encode($token,base64_decode($this->admin_api));
	} 

	public function decode_admin($jwt = ''){

    	if(is_numeric($jwt) )
			return false;
	
    	return JWT::decode($jwt,base64_decode($this->admin_api),array('HS256'));
	} */

}
