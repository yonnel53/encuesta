<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH ."/third_party/JWT/auto_loadjwt.php";
use JWT\PHPJWT;

class Form extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->JWT = new PHPJWT();
    }

	public function index()
	{
		$parametros['data_client'] = (object)[
			'token_client' => $this->JWT->encode_func()
		];
		$this->load->view('intro',$parametros);
	}

	
	public function ResponseForm($token = null){
		
		if($token == null || $token == '' || !$this->JWT->decode_func($token)){
			header('HTTP/1.1 400 Usted no tiene Acceso');
			exit(0);
		}

		$parametos['data_client'] = (object)[
			'nombres'     => '',
			'apellido_p'  => '',
			'name_clinic' => 'AG2',
			'img_profile' => 'ag2.jpg',
			'id_cliente' => '1',
			'id_user_asesora' => '1'
		];

		$this->load->view('form',$parametos);
	}

	public function SaveQuest(){

		$all_ouput     = [];
		$nowForm       = $this->input->post();
		$nowForm['ip'] = getIp();

		if($_SERVER['REQUEST_METHOD'] != 'POST'){
			header('HTTP/1.1 500 Usted no tiene Acceso');
			exit(0);
		}

		if(file_exists("./assets/question/question.json")){
			$all_ouput = json_decode(file_get_contents('./assets/question/question.json'),true);
			
			if( (!$all_ouput && $all_ouput = []) || ( count($all_ouput) == 0) || $all_ouput)
				array_push($all_ouput,$nowForm);
			
		}else{

			if(!file_exists("./assets/question/"))
				mkdir("./assets/question/",0755,true);

			array_push($all_ouput,$nowForm);
		}
		
		file_put_contents('./assets/question/question.json',json_encode($all_ouput));
		echo json_encode(['status' => true]);
	}

	public function GetJson($token = null){

		if($token == null || $token == '' || !$this->JWT->decode_func($token,true)){
			header('HTTP/1.1 400 Usted no tiene Acceso');
			exit(0);
		}

		echo file_get_contents('./assets/question/question.json');
	}

	
}
