<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>#Encuesta de Satisfaccion</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="colorlib.com">

        <!-- MATERIAL DESIGN ICONIC FONT -->
		<link rel="stylesheet" href="<?= base_url() ?>assets/css/material-design-iconic-font.css">
		<!-- STYLE CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
        
        <!-- STAR-RATING CSS -->
        <link rel="stylesheet" href="<?=base_url()?>assets/css/star-rating.css">
        <link href="<?=base_url()?>assets/vendor/sweetalert.css" rel="stylesheet">

        <style>
            .gl-star-rating-text {
                display: inline-block;
                position: relative;
                height: 34px;
                line-height: 34px;
                font-size: 14px;
                font-weight: 400;
                color: #fff !important;
                background-color: #212121 !important;
                white-space: nowrap;
                vertical-align: middle;
                padding: 0 12px 0 6px;
                margin: 0 0 0 185px;
            }

            .image-holder {
                align-self: center;
                text-align: center;
            }


            .image-holder img{
                border-radius: 100%;
                width: 300px;
                height: 300px;
            }



            .actions li a {
                padding: 0;
                border: none;
                display: inline-flex;
                height: 54px;
                width: 189px;
                letter-spacing: 1.3px;
                align-items: center;
                background: #e4bd37;
                font-family: "Muli-Bold";
                cursor: pointer;
                position: relative;
                padding-left: 34px;
                text-transform: uppercase;
                color: #fff;
                border-radius: 27px;
                -webkit-transform: perspective(1px) translateZ(0);
                transform: perspective(1px) translateZ(0);
                -webkit-transition-duration: 0.3s;
                transition-duration: 0.3s;
            }

            @media (max-width: 991px){
                .image-holder, .wrapper {
                    display: block !important;
                }

                .image-holder{
                    width: 100%;
                }

                .image-holder img{
                    width: 100px;
                    height: 100px;
                    margin-bottom: 100px;
                }
            }

            body{
                color : #ffff !important;
            }


        </style>
    </head>
	<body class="hide_body">
		<div class="wrapper initform">
			<div class="image-holder">
                <div class="form-header">
            		<h3><?=  $data_client->nombres ?> <?=  $data_client->apellido_p ?></h3> 
                </div>
				<img src="<?=base_url()?>assets/img/profile/<?=  $data_client->img_profile ?>" alt="">
            </div>
            <form action="" class="form_save">
            	<div class="form-header">
            		<a href="#">#Encuesta de Satisfacción</a>
                </div>
                <br><br>
            	<div id="wizard">
                    <form action="af" method="POST" id="formss">
                        <input type="hidden" id="name_clinic" value="<?= $data_client->name_clinic ?>">
                        <h4></h4>
                        <section>
                            <div class="form-row" style="margin-bottom: 26px;">
                                <h4>1 - Cómo calificas nuestros servicios de Diseños Gráficos, Redes Sociales y la actitud de servicio del personal que te atiende:</h4>
                            </div>

                            <div class="form-row" style="margin-bottom: 26px;">
                            
                                <div class="form-field" style="width: 100%">
                                    <select name="question1" id="question1" class="star-rating">
                                        <option value="">Select a rating</option>
                                        <option value="5">Exelente</option>
                                        <option value="4">Muy Bueno</option>
                                        <option value="3">Bueno</option>
                                        <option value="2">Regular</option>
                                        <option value="1">Malo</option>
                                    </select>
                                </div>
                            </div>
                        </section>
                        <h4></h4>
                        <section>
                            <div class="form-row" style="margin-bottom: 26px;">
                                <h4>2 - Cómo calificas nuestros servicios del ContPaq, Nomina, Facturación, Bancos, SmartSheet, Office 365, MS Teams y la actitud de servicio del personal que te atiende: </h4>
                            </div>
                            <div class="form-row" style="margin-bottom: 26px;">
                                <div class="form-field" style="width: 100%">
                                    <select name="question2" id="question2" class="star-rating">
                                        <option value="">Select a rating</option>
                                        <option value="5">Exelente</option>
                                        <option value="4">Muy Bueno</option>
                                        <option value="3">Bueno</option>
                                        <option value="2">Regular</option>
                                        <option value="1">Malo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row" style="margin-bottom: 26px;">
                                <div style="width: 100%">
                                    <h4>
                                        Observaciones :
                                    </h4>
                                    <textarea style="color: black; background-color:#ffff" name="observaciones2" id="observaciones2"></textarea>
                                </div>
                            </div>
                        </section>
                        <h4></h4>
                        <section>
                            <div class="form-row" style="margin-bottom: 26px;">
                                <h4>3 - Cómo calificas nuestros servicios de soporte técnico de los equipos de cómputos (Desktop, Portátiles, Impresoras) y la actitud de servicio del personal que te atiende:</h4>
                            </div>
                            <div class="form-row" style="margin-bottom: 26px;">
                                <div class="form-field" style="width: 100%">
                                    <select name="question3" id="question3" class="star-rating">
                                        <option value="">Select a rating</option>
                                        <option value="5">Exelente</option>
                                        <option value="4">Muy Bueno</option>
                                        <option value="3">Bueno</option>
                                        <option value="2">Regular</option>
                                        <option value="1">Malo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row" style="margin-bottom: 26px;">
                                <div style="width: 100%">
                                    <h4>
                                        Observaciones :
                                    </h4>
                                    <textarea style="color: black; background-color:#ffff" name="observaciones3" id="observaciones3"></textarea>
                                </div>
                            </div>
                        </section>
                         <h4></h4>
                        <section>
                            <div class="form-row" style="margin-bottom: 26px;">
                                <h4>4 - Cómo calificas nuestros servicios de mantenimiento, desarrollos y capacitación de los sistemas: Gm2-CRM Ventas Inmobiliarias, Sistema Integral de Pago (SIP) , G-Talent (Gestión de Talentos),  Cwork (Corworking Space / UrbanHub), Sport Academy Training System (Academia Baaxal) y la actitud de servicio del personal que te atiende:</h4>
                            </div>
                            <div class="form-row" style="margin-bottom: 26px;">
                                <div class="form-field" style="width: 100%">
                                    <select name="question4" id="question4" class="star-rating">
                                        <option value="">Select a rating</option>
                                        <option value="5">Exelente</option>
                                        <option value="4">Muy Bueno</option>
                                        <option value="3">Bueno</option>
                                        <option value="2">Regular</option>
                                        <option value="1">Malo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row" style="margin-bottom: 26px;">
                                <div style="width: 100%">
                                    <h4>
                                        Observaciones :
                                    </h4>
                                    <textarea style="color: black; background-color:#ffff" name="observaciones4" id="observaciones4"></textarea>
                                </div>
                            </div>
                        </section>
                         <h4></h4>
                        <section>
                            <div class="form-row" style="margin-bottom: 26px;">
                                <h4>5 - Cómo calificas la gestión de la Dirección de IT en las diferentes áreas de la corporación, seguimientos de proyectos y proveedores, mejora de procesos de negocios, seguridad informática, evaluación de las alternativas tecnológicas y la actitud de servicio del personal que te atiende:</h4>
                            </div>
                            <div class="form-row" style="margin-bottom: 26px;">
                                <div class="form-field" style="width: 100%">
                                    <select name="question5" id="question5" class="star-rating">
                                        <option value="">Select a rating</option>
                                        <option value="5">Exelente</option>
                                        <option value="4">Muy Bueno</option>
                                        <option value="3">Bueno</option>
                                        <option value="2">Regular</option>
                                        <option value="1">Malo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row" style="margin-bottom: 26px;">
                                <div style="width: 100%">
                                    <h4>
                                        Observaciones :
                                    </h4>
                                    <textarea style="color: black; background-color:#ffff" name="observaciones5" id="observaciones5"></textarea>
                                </div>
                            </div>
                        </section>
                        <!--
                        <input type="hidden" name="id_client" id="id_client" value="<?= $data_client->id_cliente ?>">
                        <input type="hidden" name="id_user" id="id_user" value="<?= $data_client->id_user_asesora ?>">-->
                    </form>
            	</div>
            </form>
        </div>
        <div class="culminate wrapper">
            <form action="" method="POST" style="margin: 0 auto;">
                <div class="form-header">
                    <a href="#" onclick="void:(0)">#Encuesta Completada</a>
                </div>
                <br><br>
                <div id="wizzardComplete" style="padding-bottom : 50px;">
                    <h4></h4>
                    <section>
                        <br><br>
                        <div class="form-row" style="margin-bottom: 26px;">
                        <h4 style=" text-align: justify; font-size : 20px; color : #ffff">
                            Usted ya ha completado la encuesta, Agradecemos su colaboracion
                        </h4>
                    </section>
                    </div>
                </div>  
            </form>
        </div>
        <div class="PrivateBrowser wrapper">
            <form action="" method="POST" style="margin: 0 auto;">
                <div class="form-header">
                    <a href="#" onclick="void:(0)">#Advertencia</a>
                </div>
                <br><br>
                <div id="wizzardComplete" style="padding-bottom : 50px;">
                    <h4></h4>
                    <section>
                        <br><br>
                        <div class="form-row" style="margin-bottom: 26px;">
                        <h4 style=" text-align: justify; font-size : 20px; color : #ffff">
                            No puede realizar esta encuesta si su navegador se encuentra en modo Incognito
                        </h4>
                    </section>
                    </div>
                </div>  
            </form>
        </div>
        <!---->
		<script src="<?=base_url()?>assets/js/jquery-3.3.1.min.js"></script>
		
		<!-- JQUERY STEP -->
		<script src="<?=base_url()?>assets/js/jquery.steps.js"></script>
        
        <!-- -->
        <script src="<?= base_url(); ?>assets/js/PrivateBrowser.js"></script>

        <script src="<?=base_url()?>assets/js/star-rating.js?ver=3.2.0"></script>

        <!--my js -->
        <script src="<?=base_url()?>assets/js/html2canvas.min.js"></script>
        <script src="<?=base_url()?>assets/js/FingerPrinter.js"></script>
        
        <!-- -->
        <script src="<?=base_url()?>assets/js/main.js"></script>
        <script src="<?=base_url()?>assets/vendor/sweetalert.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/vendor/sweetalert-dev.js" type="text/javascript"></script>
        <!-- -->

        <!--
        <script src="<?=base_url()?>assets/js/Performance/PerformanceAjax.js" type="text/javascript"></script>
        -->

	<script>
		
        
        $(document).ready(function(){

            var destroyed = false;

            var starratings = new StarRating( '.star-rating', {
                onClick: function( el ) {},
                'initialText' : 'Califique'
            });

            $(".actions a[href$='#finish']").click(function (e) { 

                if(localStorage.getItem('idResponse'))
                    return;

                const formdata = {
                    'question1'  : {
                        'response'    : $("#question1").val(),
                        'observacion' : '',
                    },
                    'question2'  : {
                        'response'    : $("#question2").val(),
                        'observacion' : $('#observaciones2').val(),
                    },
                    'question3'  : {
                        'response'    : $("#question3").val(),
                        'observacion' : $('#observaciones3').val(),
                    },
                    'question4'  : {
                        'response'    : $("#question4").val(),
                        'observacion' : $('#observaciones4').val(),
                    },
                    'question5'  : {
                        'response'    : $("#question5").val(),
                        'observacion' : $('#observaciones5').val(),
                    },
                    'question6'  : {
                        'response'    : $("#question6").val(),
                        'observacion' : $('#observaciones6').val(),
                    },
     
                }

   
                var Finger = new FingerPrinter();

                Finger.getPrinterSave().then( (data) => {

                    var id = data.id;
                    formdata.UserIdenti = data;
          
                    $.ajax({
                        "url"      :  '<?=base_url()?>Form/SaveQuest',
                        "type"     :  "POST",
                        "dataType" : 'JSON',
                        "data"     :  formdata,
                        success : function(response){
                            localStorage.setItem('idResponse',id);

                            swal(
                                `En ${$("#name_clinic").val()},
                                agradecemos su calificación, esto con el fin de mejorar para ustedes`, 
                                "Encuesta Enviada", 
                                "success"
                            );

                            setTimeout(() =>  location.reload(), 3000);
                        } 
                    })
                    
                   /*
     
                    var options = {
                        url      :  '<?=base_url()?>/Form/SaveQuest',
                        type     :  "POST",
                        dataType : 'JSON',
                        body     :  formdata,
                    };
                    
                    Axios(
                        options,
                        () => {

                            localStorage.setItem('idResponse',id);

                            swal(
                                `En ${$("#name_clinic").val()},
                                agradecemos su calificación, esto con el fin de mejorar para ustedes`, 
                                "Encuesta Enviada", 
                                "success"
                            );

                            setTimeout(() =>  location.reload(), 3000);
                            
                        }
                    );*/

                })

            });

        });
  
	</script>
       
</body>
</html>





			


	



