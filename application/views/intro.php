<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8">
		<title>#Encuesta de Satisfaccion</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="colorlib.com">
		<!-- MATERIAL DESIGN ICONIC FONT -->
		<link rel="stylesheet" href="<?= base_url() ?>assets/css/material-design-iconic-font.css">
		<!-- STYLE CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
        <style>
            .actions li:last-child a {
                width: 163px;
            }
            @media (max-width: 767px){
                .wrapper {
                    height: 700px; 
                    padding: 30px 20px;
                }

                .culminate h4{
                    line-height: 1.5rem;
                    font-weight: 700;
                    letter-spacing: -0.03125em !important;
                }
            }
        </style>
    </head>
	<body class="hide_body">   
		<div class="wrapper">
            <form action="" style="margin: 0 auto;">
            	<div class="form-header">
            		<a href="#">#Encuesta de Satisfacción</a>
            		<!--<h3>Register for the course online</h3> -->
                </div>
                <br><br>
            	<div id="wizard2">
                    <form action="af" method="POST" id="formss">
                        <!-- SECTION 1 -->
                        <h4></h4>
                        <section>
                            <!--   $data_client->name_client ?>-->
                            <h2>Bienvenid@</h2>
                            <br><br>
                            <div class="form-row" style="margin-bottom: 26px;">
                            <!--   $data_client->name_clinic ?>-->
                            <h4 style=" text-align: justify;">Como parte del proceso de mejora continua  de la calidad de servicios de TI en las diferentes áreas de la corporación, pedimos su apoyo para la calificación de nuestros servicios, así como los valiosos comentarios que nos brindes, los cuales serán de gran utilidad para mejorarlos.
                            </h4>
                            <div class="form-row" style="margin-bottom: 26px;"> </div>
                        </section>
                            <!--   $data_client->id_cliente ?>-->
                                <!--   $data_client->id_user_asesora ?>-->
                            <input type="hidden" name="token_id" id="token_client" value="<?= $data_client->token_client; ?>">
                 <!--          <input type="hidden" name="id_user" id="id_user" value="2"> -->
                        </div>
                    </form>
                </div>  
            </form>
        </div>
        <div class="culminate wrapper">
            <form action="" method="POST" style="margin: 0 auto;">
                <div class="form-header">
                    <a href="#" onclick="void:(0)">#Encuesta Completada</a>
                </div>
                <br><br>
                <div id="wizzardComplete" style="padding-bottom : 50px;">
                    <h4></h4>
                    <section>
                        <br><br>
                        <div class="form-row" style="margin-bottom: 26px;">
                        <h4 style=" text-align: justify; font-size : 20px; color : #ffff;letter-spacing: -0.03125em !important;">
                            Usted ya ha completado la encuesta, Agradecemos su colaboracion
                        </h4>
                    </section>
                    </div>
                </div>  
            </form>
        </div>
        <div class="PrivateBrowser wrapper">
            <form action="" method="POST" style="margin: 0 auto;">
                <div class="form-header">
                    <a href="#" onclick="void:(0)">#Advertencia</a>
                </div>
                <br><br>
                <div id="wizzardComplete" style="padding-bottom : 50px;">
                    <h4></h4>
                    <section>
                        <br><br>
                        <div class="form-row" style="margin-bottom: 26px;">
                        <h4 style=" text-align: justify; font-size : 20px; color : #ffff">
                            No puede realizar esta encuesta si su navegador se encuentra en modo Incognito
                        </h4>
                    </section>
                    </div>
                </div>  
            </form>
        </div>
        
		<script src="<?= base_url(); ?>assets/js/jquery-3.3.1.min.js"></script>
		
		<script src="<?= base_url(); ?>assets/js/jquery.steps.js"></script>
        
        <script src="<?= base_url(); ?>assets/js/star-rating.js?ver=3.2.0"></script>

        <script src="<?= base_url(); ?>assets/js/PrivateBrowser.js"></script>

        <script src="<?= base_url(); ?>assets/js/main.js"></script>
        
        <script>
        
            $(document).ready(function(){
                var destroyed = false;
                var starratings = new StarRating( '.star-rating', {
                    onClick: function( el ) {
                        console.log( 'Selected: ' + el[el.selectedIndex].text );
                    },
                    'initialText' : 'Califique'
                });

                $(".actions a[href$='#finish']").click(function (e) { 
    
                    const data = {
                        'token_client' : $("#token_client").val()
                    }

                    window.location.href = `<?= base_url(); ?>Form/ResponseForm/${data.token_client}`;
                });

            });

        </script>
    <!-- Template created and distributed by Colorlib -->
    </body>
</html>





			


	



